<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
  return view('moduleselectorpage');
});
Route::Resource('/states','GStateController');
Route::Resource('/districts','GDistrictController');
Route::Resource('/taluks','GTalukController');
Route::Resource('/blocks','GBlockController');
Route::Resource('/panchayaths','GPanchayathController');
Route::Resource('/wards','GWardController');
Route::Resource('/addresses','GAddressController');
Route::Resource('/organizations','GOrganizationController');






//test route
Route::get('/test', function () {
    return view('template');
});
