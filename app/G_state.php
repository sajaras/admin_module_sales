<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class G_state extends Model
{

    protected $table = 'g_state';
    protected $fillable = ['id','name'];
}
