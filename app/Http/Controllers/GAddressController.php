<?php

namespace App\Http\Controllers;

use App\G_Address;
use Illuminate\Http\Request;

class GAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data=null;   //fetch data when you are doing this part
      return view('g_address',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\G_Address  $g_Address
     * @return \Illuminate\Http\Response
     */
    public function show(G_Address $g_Address)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\G_Address  $g_Address
     * @return \Illuminate\Http\Response
     */
    public function edit(G_Address $g_Address)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\G_Address  $g_Address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, G_Address $g_Address)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\G_Address  $g_Address
     * @return \Illuminate\Http\Response
     */
    public function destroy(G_Address $g_Address)
    {
        //
    }
}
