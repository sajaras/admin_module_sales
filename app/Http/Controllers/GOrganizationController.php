<?php

namespace App\Http\Controllers;

use App\G_organization;
use Illuminate\Http\Request;

class GOrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data=null;   //fetch data when you are doing this part
      return view('g_organization',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\G_organization  $g_organization
     * @return \Illuminate\Http\Response
     */
    public function show(G_organization $g_organization)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\G_organization  $g_organization
     * @return \Illuminate\Http\Response
     */
    public function edit(G_organization $g_organization)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\G_organization  $g_organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, G_organization $g_organization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\G_organization  $g_organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(G_organization $g_organization)
    {
        //
    }
}
