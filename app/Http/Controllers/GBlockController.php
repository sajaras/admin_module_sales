<?php

namespace App\Http\Controllers;

use App\G_block;
use Illuminate\Http\Request;

class GBlockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data=null;   //fetch data when you are doing this part
      return view('g_block',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\G_block  $g_block
     * @return \Illuminate\Http\Response
     */
    public function show(G_block $g_block)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\G_block  $g_block
     * @return \Illuminate\Http\Response
     */
    public function edit(G_block $g_block)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\G_block  $g_block
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, G_block $g_block)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\G_block  $g_block
     * @return \Illuminate\Http\Response
     */
    public function destroy(G_block $g_block)
    {
        //
    }
}
