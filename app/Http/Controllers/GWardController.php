<?php

namespace App\Http\Controllers;

use App\G_ward;
use Illuminate\Http\Request;

class GWardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data=null;   //fetch data when you are doing this part
      return view('g_ward',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\G_ward  $g_ward
     * @return \Illuminate\Http\Response
     */
    public function show(G_ward $g_ward)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\G_ward  $g_ward
     * @return \Illuminate\Http\Response
     */
    public function edit(G_ward $g_ward)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\G_ward  $g_ward
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, G_ward $g_ward)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\G_ward  $g_ward
     * @return \Illuminate\Http\Response
     */
    public function destroy(G_ward $g_ward)
    {
        //
    }
}
