<?php

namespace App\Http\Controllers;

use App\G_panchayath;
use Illuminate\Http\Request;

class GPanchayathController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data=null;   //fetch data when you are doing this part
      return view('g_panchayath',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\G_panchayath  $g_panchayath
     * @return \Illuminate\Http\Response
     */
    public function show(G_panchayath $g_panchayath)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\G_panchayath  $g_panchayath
     * @return \Illuminate\Http\Response
     */
    public function edit(G_panchayath $g_panchayath)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\G_panchayath  $g_panchayath
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, G_panchayath $g_panchayath)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\G_panchayath  $g_panchayath
     * @return \Illuminate\Http\Response
     */
    public function destroy(G_panchayath $g_panchayath)
    {
        //
    }
}
