@extends('erp-layout.sitelayout')


@section('thispageleftsidebar')

@include('leftsidebars.general_lsb')

@endsection

@section('thispagehtml')
<div class="container-fluid contentwrapper" id="refreshblock">
<br>
<br>
<!-- your code goes here with div class row (preffered) as starting -->
<div class="row">
  <div class="col-sm-offset-2 col-sm-2">
    <button id="addbutton" data-toggle="modal" data-target="#addmodal" class="btn schemecolor3bg schemecolor1fg"><i class=" fa fa-plus"></i> Add</button>
  </div>
    <div class=" col-sm-3">

    <input type="search" id="tablesearchbox" placeholder="Search Here" class="form-control">

    </div>
</div>
<br>
<div class="row">
<div class="col-sm-offset-1 col-sm-10">
      <table id="thispagetable" class="table table-hover table-bordered ">

          <thead class="schemecolor3bg schemecolor1fg">
            <tr class="Head">

                    <td  class="col-sm-6 text-center">ID</td>
                    <td class="col-sm-6 text-center">NAME</td>

            </tr>
          </thead>
          <tbody>
              @if($data->count() > 0)
              @foreach($data as $eachdata)
                <tr >
                  <td id="col1" class="col-sm-6 text-center thisrow">{{$eachdata->id}}</td>
                  <td id="col2" class="col-sm-6 text-center thisrow">{{$eachdata->name}}</td>
                </tr>
              @endforeach
              @else
              <tr class="text-center">No Data Found</tr>
              @endif
          </tbody>

      </table>

</div>
</div>


</div>

<!-- Add Modal -->
<div id="addmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center schemecolor3fg"><b>ADD STATE</b></h4>
      </div>
      <div class="modal-body">
        <div class="row">

  <form id="thisform" class="form-horizontal">
      {{ csrf_field() }}
  <div class="form-group">
    <label for="field1" class="col-sm-2 control-label">ID</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="field1" name="id" placeholder="Enter State id">
      <strong id="field1-error" class="text-center text-danger"></strong>
    </div>
  </div>
  <div class="form-group">
    <label for="field2" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="field2" name="name" placeholder="Enter Name">
      <strong  id="field2-error" class="text-center text-danger"></strong>
    </div>
  </div>






        </div>

      </div>
      <div class="modal-footer">
        <span id="successmessage" class="text-success pull-left" style="display:none;">Data Added Successfully</span>
        <button id="saveandnextbutton"  type="button" class="btn schemecolor2bg schemecolor1fg">Save &amp; Next</button>
        <button id="saveandexitbutton" type="button" class="btn schemecolor2bg schemecolor1fg">Save &amp; Exit</button>
      </form>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- edit Modal -->
<div id="editmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center schemecolor3fg"><b>EDIT STATE</b></h4>
      </div>
      <div class="modal-body">
        <div class="row">

  <form id="thiseditform" class="form-horizontal">
      {{ csrf_field() }}
  <div class="form-group">
    <label for="editfield1" class="col-sm-2 control-label">ID</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="editfield1" name="id" placeholder="Enter State id">
      <strong id="editfield1-error" class="text-center text-danger"></strong>
    </div>
  </div>
  <div class="form-group">
    <label for="editfield2" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="editfield2" name="name" placeholder="Enter Name">
      <strong  id="editfield2-error" class="text-center text-danger"></strong>
    </div>
  </div>
<input type="hidden" id="thisrowoldid" >
<input type="hidden" id="thisrowfield2" >





        </div>

      </div>
      <div class="modal-footer">
        <span id="editsuccessmessage" class="text-success pull-left" style="display:none;">Data Updated Successfully</span>
        <button id="updatebutton"  type="button" class="btn schemecolor2bg schemecolor1fg">Update</button>
        <button id="deletebutton" type="button" class="btn schemecolor2bg schemecolor1fg" >Delete</button>
      </form>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- edit Modal -->
<div id="deletemodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center schemecolor3fg"><b>Confirmation</b></h4>
      </div>
      <div class="modal-body">

              <div class="row">
              <div class="col-sm-offset-2 col-sm-8">
                <h4 class="text-danger">Are you sure to delete <span id="entityname"></span> with id <span id="entityid"></span>?</h4>
                <h3 class="text-center">Details</h3>
                <hr>
                <p class="text-center"><span id="deletefield1"></span><span id="deletefield1value"></span></p>
                <p class="text-center"><span id="deletefield2"></span><span id="deletefield2value"></span></p>
                <p></p>
              </div>
              </div>


      </div>

      <div class="modal-footer">
        <button id="confirmyesbutton"  type="button" class="btn schemecolor2bg schemecolor1fg">YES</button>
        <button id="nobutton"  type="button" class="btn schemecolor2bg schemecolor1fg" data-dismiss="modal">NO</button>

      </div>
    </div>

  </div>
</div>
@endsection


@section('thispagejavascript_jquery')
<script type="text/javascript">

$(document).ready(function(){
  $('#tablesearchbox').on('keyup', function() {
   var value = $(this).val();
   var patt = new RegExp(value, "i");

  $('#thispagetable').find('tr').each(function() {
     if (!($(this).find('td').text().search(patt) >= 0)) {
      $(this).not('.Head').hide();
  }
  if (($(this).find('td').text().search(patt) >= 0)) {
    $(this).show();
  }
});

});

//add button functionality

$("#saveandnextbutton").on("click",function(){
// console.log('i am here');
var form = $("#thisform");
var formdata = form.serialize();
console.log(formdata);
$.ajax({
  url:"{{ route('states.store') }}",
  type:'post',
  data:formdata,
  success:function(data) {
        if(data == 1)
        {
            $('#refreshblock').load(location.href + ' #refreshblock');
            $('#successmessage').show();
            $('#successmessage').fadeOut(4000);
            $('#field1').val("");
            $('#field2').val("");


        }
        else if(data.errors) {
          if(data.errors.id)
          {
            $("#field1-error").html(data.errors.id[0]);
          }
          if(data.errors.name){
            $("#field2-error").html(data.errors.name[0]);

            }
        }
        else {

        }


},

});

});

$("#saveandexitbutton").on("click",function(){
// console.log('i am here');
var form = $("#thisform");
var formdata = form.serialize();
console.log(formdata);
$.ajax({
  url:"{{ route('states.store') }}",
  type:'post',
  data:formdata,
  success:function(data) {
        if(data == 1)
        {
            $('#refreshblock').load(location.href + ' #refreshblock');

            $('#field1').val("");
            $('#field2').val("");
            $("#addmodal").modal('hide');


        }
        else if(data.errors) {
          if(data.errors.id)
          {
            $("#field1-error").html(data.errors.id[0]);
          }
          if(data.errors.name){
            $("#field2-error").html(data.errors.name[0]);

            }
        }
        else {

        }


},

});

});
//end of add functionality

//start of edit functionality
$(document).on('dblclick','.thisrow',function(){

  var field1 = $(this).closest('tr').find('#col1').text();
  var field2 = $(this).closest('tr').find('#col2').text();
  $('#thisrowoldid').val(field1);
  $('#thisrowfield2').val(field2);


  $('#editfield1').val(field1);
  $('#editfield2').val(field2);

   $('#editmodal').modal('show');


});


//update functionality
$('#updatebutton').click(function(){
  console.log('hi');
  var oldid = $('#thisrowoldid').val();
  var newid = $('#editfield1').val();

  var editform = $("#thiseditform");
  var formdata = editform.serialize();
  url = "/states/" + oldid;

  $.ajax({
    url:url,
    type:'put',
    data:formdata,
    success:function(data) {

          if(data == 1)
          {
              $('#refreshblock').load(location.href + ' #refreshblock');
              $('#editmodal').modal('hide');

          }

          else if(data.errors) {
            if(data.errors.id)
            {
              $("#editfield1-error").html(data.errors.id[0]);
            }
            if(data.errors.name){
              $("#editfield2-error").html(data.errors.name[0]);

              }
          }
          else {

          }


  },

  });

});

$('#deletebutton').click(function(){

 var id = $('#thisrowoldid').val();
 var field2 = $('#thisrowfield2').val();
 $("#entityid").text(id);
 $("#entityname").text("State");
$('#deletefield1').val('Id');
$('#deletefield1value').text(id);
$('#deletefield2value').text(field2);
$('#deletemodal').modal('show');


});

//update functionality
$('#confirmyesbutton').click(function(){

var id =$('#entityid').text();
console.log(id);

  url = "/states/" + id;

  $.ajax({
    url:url,
    type:'delete',
    data:{'id':id,_method:'post',_token:$('input[name=_token]').val()},
    success:function(data) {

          if(data == 1)
          {
              $('#refreshblock').load(location.href + ' #refreshblock');

              $('#editmodal').modal('hide');
              $('#deletemodal').modal('hide');

          }


          else {

          }


  },

  });

});

});




 </script>

@endsection
