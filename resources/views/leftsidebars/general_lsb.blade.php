<!-- General menu left navigation bar -->
      <nav class="navbar navbar-inverse erpsidebar schemecolor3bg navbar-fixed-left" style="top:8%; z-index:10;border:none;" >
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header ">
      <button type="button" class="pull-left navbar-toggle collapsed schemecolor1bg" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <!-- <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> -->
        <span><i class="fa fa-chevron-circle-down schemecolor2fg" aria-hidden="true"></i>
</span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav text-center">
        <li ><a href="{{route('organizations.index')}}" class="sidebarlink schemecolor1fg">MANAGE ORGANISATION <span class="sr-only">(current)</span></a></li>
          <li ><a href="{{route('addresses.index')}}" class="sidebarlink schemecolor1fg">ADDRESS BOOK</a></li>
      <li class="accordion">ADDRESS MASTERS<span><i style="margin-left:5px;" class="fa fa-sort-desc fa-1x"  aria-hidden="true"></i></span></li>
  <div class="panel text-center schemecolor2bg schemecolor1fg">
    <li style="padding:10px;"><a class="schemecolor1fg" href="{{route('states.index')}}">MANAGE STATES</a></li>
    <li style="padding:10px;"><a class="schemecolor1fg" href="{{route('districts.index')}}">MANAGE DISTRICTS</a></li>
    <li style="padding:10px;"><a class="schemecolor1fg" href="{{route('taluks.index')}}">MANAGE TALUKS</a></li>
    <li style="padding:10px;"><a class="schemecolor1fg" href="{{route('blocks.index')}}">MANAGE BLOCKS</a></li>
    <li style="padding:10px;"><a class="schemecolor1fg" href="{{route('panchayaths.index')}}">MANAGE PANCHAYATHS</a></li>
    <li style="padding:10px;"><a class="schemecolor1fg" href="{{route('wards.index')}}">MANAGE WARDS</a></li>
  </div>




      </ul>


    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- end of left navigation bar -->
