<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="_token" content="{{ csrf_token() }}">

        @yield('thispagetitle')

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type='text/css'>
        <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/navbar-fixed-left.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/accordion.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/colorscheme.css') }}" rel="stylesheet">
        @yield('thispagecss')


    </head>

    <body>



      <nav class="navbar navbar-default erptopbar schemecolor1bg schemecolor3fg navbar-fixed-top">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ERP-SALES</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">GENERAL</a></li>
              <li><a href="#">MENU ITEM</a></li>
              <li><a href="#">MENU ITEM</a></li>
              <li><a href="#">MENU ITEM</a></li>

            </ul>
        <!-- right side of the main navbar -->
            <ul class="nav navbar-nav navbar-right">

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-th fa-2x" aria-hidden="true"></i></a>
                <ul class="dropdown-menu schemecolor1bg" style="width:350px;height:400px;padding:30px;">
                    <div class="row" style="padding-top:10px;">
                      <a href="{{route('addresses.index')}}"><div class="col-sm-4 text-center">

                        <img src="theme/addressbook3.png" class="img-responsive">
                        <p>Address</p>
                    </div></a>
                      <div class="col-sm-4 text-center">
                      <img src="theme/feature2.png" class="img-responsive">
                        <!-- <i class="fa fa-book fa-4x"></i> -->
                        <p>feature 2</p>
                    </div>
                      <div class="col-sm-4 text-center">
                          <img src="theme/feature3.png" class="img-responsive">
                        <!-- <i class="fa fa-book fa-4x"></i> -->
                        <p>feature 3</p>
                    </div>


                  </div>

                    <div class="row">



                      <div class="col-sm-4 text-center">
                          <img src="theme/finance.png" class="img-responsive">
                        <!-- <i class="fa fa-book fa-4x"></i> -->
                        <p>Finance</p>
                    </div>
                      <div class="col-sm-4 text-center">
                          <img src="theme/feature2.png" class="img-responsive">
                        <!-- <i class="fa fa-book fa-4x"></i> -->
                        <p>Module 2</p>
                    </div>
                      <div class="col-sm-4 text-center">
                          <img src="theme/feature3.png" class="img-responsive">
                        <!-- <i class="fa fa-book fa-4x"></i> -->
                        <p>Module 3</p>
                    </div>
                  </div>
                    <div class="row">



                      <div class="col-sm-4 text-center">
                          <img src="theme/feature4.png" class="img-responsive">
                        <!-- <i class="fa fa-book fa-4x"></i> -->
                        <p>Finance</p>
                    </div>
                      <div class="col-sm-4 text-center">
                          <img src="theme/feature3.png" class="img-responsive">
                        <!-- <i class="fa fa-book fa-4x"></i> -->
                        <p>Module 2</p>
                    </div>
                      <div class="col-sm-4 text-center">
                          <img src="theme/feature2.png" class="img-responsive">
                        <!-- <i class="fa fa-book fa-4x"></i> -->
                        <p>Module 3</p>
                    </div>
                  </div>
                  <hr>

                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Username <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">Logout</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>


@yield('thispageleftsidebar')

@yield('thispagehtml')
     <!-- jquery  3.3.1-->
     <script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
      <!-- bootstrap js 3.3.7 -->
  <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

  <script>
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight){
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    });
  }
  </script>

@yield('thispagejavascript_jquery')


    </body>
</html>
